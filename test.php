<?php 

if (version_compare(PHP_VERSION, '5.4.0', '<')) {
    die('Need PHP >= 5.4');
}

var_export(TestClass::init());




/**
 * container class for used methods
 * 
 **/
class TestClass
{
    /**
     * var to save directory scanning result
     * @access private
     * @var array 
     **/
    private static $result; 
    
    
    /**
     * start function
     * @access public
     * @param string $dir - path to parent dir
     * @param string $format - change output format between array and JSON
     * @return mixed
     **/
    public static function init($dir=null, $format=null)
    {
        $dir = empty($dir) ? '.' : $dir; // use current dir as parent if param empty
        self::dirScan($dir);
        self::sortResult();
        
        switch ($format) {
            case "json":
                self::$result = json_encode(self::$result);
                
            default:
                return self::$result;
        }
    }
    
    
    /**
     * Scan directory and find subdirs
     * @param string $path - path to parent dir
     * @return void
     **/
    private static function dirScan($path)
    {
        foreach (scandir($path) as $item) {
            if (in_array($item, ['.', '..'])) {
                continue;
            }
            
            $itemPath = $path.'/'.$item;
            
            if (is_dir($itemPath)) {
                self::$result[$item] = self::getDirInfo($itemPath);
            }
        }
    }
    
    
    /**
     * Get needed info from dir
     * @param string $path - path to dir
     * @return array
     **/
    private static function getDirInfo($path)
    {
        $dirInfo['Size'] = 0;
        $dirInfo['filesCount'] = 0;
        $hashes = array();
        
        foreach (scandir($path) as $item) {
            $itemPath = $path.'/'.$item;
            if (is_file($itemPath)) {
                $dirInfo['Size'] += filesize ($itemPath);
                $dirInfo['filesCount']++;
                $hashes[] = md5_file($itemPath);
            }
        }
        
        $dirInfo['filesDoubles'] = self::analyseHashes($hashes);
        
        return $dirInfo;
    }
    
    
    /**
     * Count files hash doubles
     * @param array $hashes MD5 hashes from subdir files
     * @return integer
     **/
    private static function analyseHashes($hashes)
    {
        $all = count($hashes);
        $uniq = count(array_unique($hashes));
        return $all - $uniq;
    }
    
    
    /**
     * Sort result by directory size DESC
     * @uses TestClass::$result
     * @return void
     **/
    private static function sortResult()
    {
        uasort(self::$result, function($a, $b) {
            return $b['Size'] - $a['Size'];
        });
    }
    
}
