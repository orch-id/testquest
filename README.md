# README #

This repo is my test quest 

### Command line: ###

* Write a one-line command that displays the first 20 characters of a user's ssh public key: `head -c 20 ~/.ssh/id_rsa.pub`
* Write a one-line command that checks the syntax of all php files in the current folder: 
simple for current folder: `for file in *.php; do php -l $file; done`
with subfolders: `find . -name \*.php -print0 | xargs -0 -n 1 php -l`


### Git ###

* Write a git command that displays the hash of the current revision:
`git rev-parse HEAD`
* Write a git command that displays the code changes of the last 5 commits for the file index.php (in the current folder)
`git log -p -5 index.php`



### PHP ###

* `Please, look at code` [repo](https://bitbucket.org/orch-id/testquest/src/b6ae03bcf57cc99f21331d2962b7b47c26d7d2e7/test.php?at=master)